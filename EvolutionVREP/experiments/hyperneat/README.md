# HyperNEAT

This experiment is a wrapper to MultiNEAT which allows to use in the ARE Framework the HyperNEAT algorithm [1]. 

References:

[1] Risi, S., & Stanley, K. O. (2012). An enhanced hypercube-based encoding for evolving the placement, density, and connectivity of neurons. Artificial life, 18(4), 331-363.