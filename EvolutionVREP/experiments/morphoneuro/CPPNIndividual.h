#ifndef CPPNINDIVIDUAL_H
#define CPPNINDIVIDUAL_H

#include "ARE/nn2/NN2Individual.hpp"
#include "ARE/CPPNGenome.h"
#include "ARE/Morphology_CPPNMatrix.h"
#include "v_repLib.h"
#include "eigen_boost_serialization.hpp"

// Learning
#include "ARE/nn2/NN2Control.hpp"
#include "ARE/NNParamGenome.hpp"
#include "ARE/nn2/NN2Settings.hpp"
#include "ARE/Settings.h"
#include "nn2/mlp.hpp"
#include "nn2/elman.hpp"
#include "nn2/rnn.hpp"

namespace are {

//using neuron_t = nn2::Neuron<nn2::PfWSum<double>,nn2::AfSigmoidSigned<std::vector<double>>>;
//using connection_t = nn2::Connection<double>;
//using ffnn_t = nn2::Mlp<neuron_t,connection_t>;
//using elman_t = nn2::Elman<neuron_t,connection_t>;
//using rnn_t = nn2::Rnn<neuron_t,connection_t>

class CPPNIndividual : public NN2Individual
{
public :
    CPPNIndividual() : NN2Individual(){}
    CPPNIndividual(const Genome::Ptr& morph_gen,const NNParamGenome::Ptr& ctrl_gen) :
            NN2Individual(morph_gen, ctrl_gen){}
    CPPNIndividual(const CPPNIndividual& ind):
            NN2Individual(ind),
            nn(ind.nn),
            testRes(ind.testRes),
            graphMatrix(ind.graphMatrix),
            morphDesc(ind.morphDesc),
            symDesc(ind.symDesc),
            nn_inputs(ind.nn_inputs),
            nn_outputs(ind.nn_outputs)
    {}
    Individual::Ptr clone() override{
        return std::make_shared<CPPNIndividual>(*this);
    }

    template<class archive>
    void serialize(archive &arch, const unsigned int v)
    {
        arch & objectives;
        arch & ctrlGenome;
        arch & morphGenome;
        arch & final_position;
        arch & nn_inputs;
        arch & nn_outputs;

    }
    // Serialization
    std::string to_string();
    void from_string(const std::string &str);

    // Setters and getters
    NEAT::NeuralNetwork getGenome(){return nn;};

    std::vector<bool> getManRes(){return testRes;};
    double getManScore(){ return manScore;};
    void setGenome();

    void setManRes();
    void setManScore();

    /// Setters for descritors
    void setMorphDesc();
    void setGraphMatrix();
    void setSymDesc();
    void set_nn_inputs(int nni){nn_inputs = nni;}
    void set_nn_outputs(int nno){nn_outputs = nno;}
    /// Getters for descritors
    Eigen::VectorXd getMorphDesc(){return morphDesc;};
    std::vector<std::vector<std::vector<int>>> getGraphMatrix(){return graphMatrix;};
    Eigen::VectorXd getSymDesc(){return symDesc;};

    Eigen::VectorXd descriptor();


    void update(double delta_time) override;

protected:
    void createMorphology() override;

    NEAT::NeuralNetwork nn;
    std::vector<bool> testRes;
    double manScore;

    /// Descritors
    std::vector<std::vector<std::vector<int>>> graphMatrix;
    Eigen::VectorXd morphDesc;
    Eigen::VectorXd symDesc;

private:

    int nn_inputs;
    int nn_outputs;
};

}//are

#endif //CPPNINDIVIDUAL_H
