#include "MorphoNeuro.h"

using namespace are;

void MorphoNeuro::init()
{
    params = NEAT::Parameters();
    /// Set parameters for NEAT
    unsigned int pop_size = settings::getParameter<settings::Integer>(parameters,"#populationSize").value;
    int num_eval = settings::getParameter<settings::Integer>(parameters,"#numEval").value;
    params.PopulationSize = pop_size*num_eval;

    // Speciesim
    params.DynamicCompatibility = false;
    params.CompatTreshold = 2.0;
    params.YoungAgeTreshold = 15;
    params.SpeciesMaxStagnation = 100;
    params.OldAgeTreshold = 50;
    params.MinSpecies = 5;
    params.MaxSpecies = 10;
    params.RouletteWheelSelection = false;
    // Selection
    params.EliteFraction = 1.0;
    params.TournamentSize = 2;
    // Mutation parameters
    params.MutateRemLinkProb = 0.0;
    params.RecurrentProb = 0.0;
    params.OverallMutationRate = 0.01;
    params.MutateAddLinkProb = 0.01;
    params.MutateAddNeuronProb = 0.01;
    params.MutateWeightsProb = 0.01;
    params.MaxWeight = 8.0;
    params.WeightMutationMaxPower = 0.0;
    params.WeightReplacementMaxPower = 0.0;
    params.MutateActivationAProb = 0.0;
    params.ActivationAMutationMaxPower = 0.5;
    params.MinActivationA = 0.05;
    params.MaxActivationA = 6.0;
    params.MutateNeuronActivationTypeProb = 0.03;
    // Crossover
    params.SurvivalRate = 1.0;
    params.CrossoverRate = 0.0;
    params.CrossoverRate = 0.0;
    params.InterspeciesCrossoverRate = 0.0;
    // Activation functions
    params.ActivationFunction_SignedSigmoid_Prob = 0.0;
    params.ActivationFunction_UnsignedSigmoid_Prob = 0.0;
    params.ActivationFunction_Tanh_Prob = 0.0;
    params.ActivationFunction_TanhCubic_Prob = 0.0;
    params.ActivationFunction_SignedStep_Prob = 0.0;
    params.ActivationFunction_UnsignedStep_Prob = 0.0;
    params.ActivationFunction_SignedGauss_Prob = 0.0;
    params.ActivationFunction_UnsignedGauss_Prob = 0.0;
    params.ActivationFunction_Abs_Prob = 0.0;
    params.ActivationFunction_SignedSine_Prob = 0.0;
    params.ActivationFunction_UnsignedSine_Prob = 0.0;
    params.ActivationFunction_Linear_Prob = 0.0;

    initPopulation();
}

void MorphoNeuro::initPopulation()
{
    int manufacturabilityMethod = settings::getParameter<settings::Integer>(parameters,"#manufacturabilityMethod").value;
    rng.Seed(randomNum->getSeed());

    // Learning stuff...
    int num_eval = settings::getParameter<settings::Integer>(parameters,"#numEval").value;
    float max_weight = settings::getParameter<settings::Float>(parameters,"#MaxWeight").value;
    int nbr_weights, nbr_biases;
    int nn_type = settings::getParameter<settings::Integer>(parameters,"#NNType").value;
    const int nb_input = settings::getParameter<settings::Integer>(parameters,"#NbrInputNeurones").value;
    const int nb_hidden = settings::getParameter<settings::Integer>(parameters,"#NbrHiddenNeurones").value;
    const int nb_output = settings::getParameter<settings::Integer>(parameters,"#NbrOutputNeurones").value;
    if(nn_type == settings::nnType::FFNN)
        NN2Control<ffnn_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else if(nn_type == settings::nnType::RNN)
        NN2Control<rnn_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else if(nn_type == settings::nnType::ELMAN)
        NN2Control<elman_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else {
        std::cerr << "unknown type of neural network" << std::endl;
        return;
    }
    Eigen::MatrixXd init_samples = limbo::tools::random_lhs(nbr_weights + nbr_biases,num_eval);
    std::vector<double> weights(nbr_weights);
    std::vector<double> biases(nbr_biases);

    // Morphology
    NEAT::Genome morph_genome(0, 5, 10, 6, false, NEAT::SIGNED_SIGMOID, NEAT::SIGNED_SIGMOID, 0, params, 10);
    morph_population = std::make_unique<NEAT::Population>(morph_genome, params, true, 1.0, randomNum->getSeed());

    // Bootstrap stuff
    int instance_type = settings::getParameter<settings::Integer>(parameters,"#instanceType").value;
    bool isBootstrapPopulation = settings::getParameter<settings::Boolean>(parameters,"#isBootstrapEvolution").value;
    std::vector<int> robotList;
    if(isBootstrapPopulation){
    //if(isBootstrapPopulation && (!simulator_side || instance_type == settings::INSTANCE_REGULAR)){
        robotList = listInds();
    }

    short int ctrlCounter = 0;
    short int morphCounter = 0;
    for (size_t i = 0; i < params.PopulationSize; i++)
    {
        if(i % num_eval == 0){
            if(i != 0)
                morphCounter++;
            ctrlCounter = 0;
        }
        else{
            ctrlCounter++;
        }

        // Learning stuff...
        int indCtrl = settings::getParameter<settings::Integer>(parameters,"#loadController").value;
        if(indCtrl > -1){
            loadController(indCtrl, weights,biases);
        } else{
            for(int v = 0; v < nbr_weights; v++)
                weights[v] = max_weight*(init_samples(ctrlCounter,v)*2.f - 1.f);
            for(int w = nbr_weights; w < nbr_weights+nbr_biases; w++)
                biases[w-nbr_weights] = max_weight*(init_samples(ctrlCounter,w)*2.f - 1.f);
        }


        // Bootstrap
        if(isBootstrapPopulation){
        //if(isBootstrapPopulation && (!simulator_side || instance_type == settings::INSTANCE_REGULAR)){
            NEAT::Genome indGenome;
            indGenome = loadInd(robotList[morphCounter]);
            morph_population->AccessGenomeByIndex(i) = indGenome;
        }
        // Generate random topologies
        /// \todo EB: This if statement might not be necessay here.
        if(manufacturabilityMethod < 0){ // Generate random robots
            NEAT::RNG rng;
            const misc::RandNum::Ptr rn = get_randomNum();
            rng.Seed(rn->randInt(1,100000));
            morph_population->AccessGenomeByIndex(i).Randomize_LinkWeights(params.MaxWeight,rng);
            morph_population->AccessGenomeByIndex(i).Mutate_NeuronBiases(params,rng);

        }
        // Learning...
        NNParamGenome::Ptr ctrl_gen(new NNParamGenome);
        ctrl_gen->set_weights(weights);
        ctrl_gen->set_biases(biases);
        // Morphology...
        CPPNGenome::Ptr morphgenome(new CPPNGenome(morph_population->AccessGenomeByIndex(i)));
        CPPNIndividual::Ptr ind(new CPPNIndividual(morphgenome,ctrl_gen));

        ind->set_parameters(parameters);
        ind->set_randNum(randomNum);
        population.push_back(ind);
    }
}

bool MorphoNeuro::update(const Environment::Ptr& env)
{
    endEvalTime = hr_clock::now();
    numberEvaluation++;

    if(simulator_side){
        Individual::Ptr ind = population[currentIndIndex];
        std::dynamic_pointer_cast<NN2Individual>(ind)->set_final_position(
                std::dynamic_pointer_cast<MazeEnv>(env)->get_final_position());
        std::dynamic_pointer_cast<NN2Individual>(ind)->set_trajectory(
                std::dynamic_pointer_cast<MazeEnv>(env)->get_trajectory());
    }
    return true;
}

void MorphoNeuro::epoch()
{
    int num_eval = settings::getParameter<settings::Integer>(parameters,"#numEval").value;
    short int morphCounter = 0;
    std::vector<double> indFitness;
    std::vector<double> tempFitness;
    /** MultiNEAT **/
    int manufacturabilityMethod = settings::getParameter<settings::Integer>(parameters,"#manufacturabilityMethod").value;
    int indCounter = 1; /// \todo EB: There must be a better way to do this!
    // Estimate best fitness for each morphology
    for(const auto& ind : population){
        tempFitness.push_back(ind->getObjectives().back());
        if(indCounter % num_eval == 0){
            if(indCounter != 0){
                morphCounter++;
                indFitness.push_back(*std::max_element(tempFitness.begin(),tempFitness.end()));
                tempFitness.clear();
            }
        }
        else{
        }
        indCounter++;
    }
    // Assign fitness.
    indCounter = 0;
    morphCounter = 0;
    for(const auto& ind : population){
        if(indCounter % num_eval == 0){
            if(indCounter != 0)
                morphCounter++;
        }
        morph_population->AccessGenomeByIndex(indCounter).SetFitness(indFitness[morphCounter]);
        indCounter++;
    }

    if(manufacturabilityMethod >= 0) { // Don't generate random robots
        morph_population->Epoch();
    }
}

void MorphoNeuro::setObjectives(size_t indIdx, const std::vector<double> &objectives){
    currentIndIndex = indIdx;
    population[indIdx]->setObjectives(objectives);
}

void MorphoNeuro::init_next_pop()
{
    population.clear();
    // Learning stuff...
    int num_eval = settings::getParameter<settings::Integer>(parameters,"#numEval").value;
    float max_weight = settings::getParameter<settings::Float>(parameters,"#MaxWeight").value;
    int nbr_weights, nbr_biases;
    int nn_type = settings::getParameter<settings::Integer>(parameters,"#NNType").value;
    const int nb_input = settings::getParameter<settings::Integer>(parameters,"#NbrInputNeurones").value;
    const int nb_hidden = settings::getParameter<settings::Integer>(parameters,"#NbrHiddenNeurones").value;
    const int nb_output = settings::getParameter<settings::Integer>(parameters,"#NbrOutputNeurones").value;
    if(nn_type == settings::nnType::FFNN)
        NN2Control<ffnn_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else if(nn_type == settings::nnType::RNN)
        NN2Control<rnn_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else if(nn_type == settings::nnType::ELMAN)
        NN2Control<elman_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else {
        std::cerr << "unknown type of neural network" << std::endl;
        return;
    }
    Eigen::MatrixXd init_samples = limbo::tools::random_lhs(nbr_weights + nbr_biases,num_eval);

    std::vector<double> weights(nbr_weights);
    std::vector<double> biases(nbr_biases);

    rng.Seed(randomNum->getSeed());

    // Morphology
    int manufacturabilityMethod = settings::getParameter<settings::Integer>(parameters,"#manufacturabilityMethod").value;
    short int ctrlCounter = 0;
    short int morphCounter = 0;
    for (size_t i = 0; i < params.PopulationSize; i++)
    {
        if(i % num_eval == 0){
            if(i != 0){
               // morphCounter++;
                morphCounter += num_eval;
            }

            ctrlCounter = 0;
        }
        else{
            ctrlCounter++;
        }
        // Learning
        for(int v = 0; v < nbr_weights; v++)
            weights[v] = max_weight*(init_samples(ctrlCounter,v)*2.f - 1.f);
        for(int w = nbr_weights; w < nbr_weights+nbr_biases; w++)
            biases[w-nbr_weights] = max_weight*(init_samples(ctrlCounter,w)*2.f - 1.f);

        // Generate random topologies
        /// \todo EB: This if statement might not be necessay here.
        if(manufacturabilityMethod < 0){ // Generate random robots
            NEAT::RNG rng;
            const misc::RandNum::Ptr rn = get_randomNum();
            rng.Seed(rn->randInt(1,100000));
            morph_population->AccessGenomeByIndex(i).Randomize_LinkWeights(params.MaxWeight,rng);
            morph_population->AccessGenomeByIndex(i).Mutate_NeuronBiases(params,rng);

        }
        // Learning...
        NNParamGenome::Ptr ctrl_gen(new NNParamGenome);
        ctrl_gen->set_weights(weights);
        ctrl_gen->set_biases(biases);
        // Morphology...
        CPPNGenome::Ptr morphgenome(new CPPNGenome(morph_population->AccessGenomeByIndex(morphCounter)));
        CPPNIndividual::Ptr ind(new CPPNIndividual(morphgenome,ctrl_gen));

        ind->set_parameters(parameters);
        ind->set_randNum(randomNum);
        population.push_back(ind);
    }
}

bool MorphoNeuro::is_finish()
{
    unsigned int maxGenerations = settings::getParameter<settings::Integer>(parameters,"#numberOfGeneration").value;
    return get_generation() > maxGenerations;
}

NEAT::Genome MorphoNeuro::loadInd(short genomeID)
{
    std::string loadExperiment = settings::getParameter<settings::String>(parameters,"#loadExperiment").value;
    std::cout << "Loading genome: " << genomeID << "!" << std::endl;
    std::stringstream filepath;
    filepath << loadExperiment << "/morphGenome" << genomeID;
    NEAT::Genome indGenome(filepath.str().c_str());
    return indGenome;
}

std::vector<int> MorphoNeuro::listInds()
{
    std::vector<int> robotList;
    // This code snippet was taken from: https://www.gormanalysis.com/blog/reading-and-writing-csv-files-with-cpp/
    std::string loadExperiment = settings::getParameter<settings::String>(parameters,"#loadExperiment").value;
    std::string bootstrapFile = settings::getParameter<settings::String>(parameters,"#bootstrapFile").value;
    // Create an input filestream
    std::ifstream myFile(loadExperiment+bootstrapFile);
    if(!myFile.is_open()) throw std::runtime_error("Could not open file");
    std::string line;
    int val;

    while(std::getline(myFile, line)){
        // Create a stringstream of the current line
        std::stringstream ss(line);
        // Keep track of the current column index
        int colIdx = 0;
        // Extract each integer
        while(ss >> val) {
            // Add the current integer to the 'colIdx' column's values vector
            robotList.push_back(val);
            // If the next token is a comma, ignore it and move on
            if(ss.peek() == ',') ss.ignore();
            // Increment the column index
            colIdx++;
        }
    }
    // Close file
    myFile.close();
    return robotList;
}

void MorphoNeuro::loadController(int ind, std::vector<double> &weights, std::vector<double> &biases)
{
    std::string repository = settings::getParameter<settings::String>(parameters,"#controllerRepository").value;

    std::stringstream sstr;
    sstr << repository << "/genome_" << 0 << "_" << ind;

    EmptyGenome::Ptr morph_gen(new EmptyGenome);
    NNParamGenome::Ptr genome;

    genome.reset(new NNParamGenome(randomNum,parameters));
    NNParamGenome::Ptr nngenome = std::dynamic_pointer_cast<NNParamGenome>(genome);
    std::ifstream logFileStream;
    logFileStream.open(sstr.str());
    std::string line;
    std::getline(logFileStream,line);
    int nbr_weights = std::stoi(line);
    std::getline(logFileStream,line);
    int nbr_bias = std::stoi(line);

    //std::vector<double> weights;
    for(int i = 0; i < nbr_weights; i++){
        std::getline(logFileStream,line);
        weights.push_back(std::stod(line));
    }
    nngenome->set_weights(weights);

    for(int i = 0; i < nbr_bias; i++){
        std::getline(logFileStream,line);
        biases.push_back(std::stod(line));
    }
    nngenome->set_biases(biases);
}
