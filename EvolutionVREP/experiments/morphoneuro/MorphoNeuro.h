#ifndef MorphoNeuro_H
#define MorphoNeuro_H

#include "ARE/EA.h"
#include "ARE/CPPNGenome.h"
#include "ARE/Morphology_CPPNMatrix.h"
#include "CPPNIndividual.h"

#include "eigen3/Eigen/Core"
#include "ARE/learning/Novelty.hpp"

// Learning stuff...
#include <limbo/init/lhs.hpp>
#include "ARE/nn2/NN2Settings.hpp"
#include "ARE/Settings.h"
#include "ARE/nn2/NN2Individual.hpp"

namespace are {

class MorphoNeuro : public EA
{
public:
    MorphoNeuro() : EA(){}
    MorphoNeuro(const settings::ParametersMapPtr& param) : EA(param){}
    ~MorphoNeuro() override {}

    void init() override;
    void initPopulation();
    void epoch() override;
    bool is_finish() override;
    void setObjectives(size_t indIdx, const std::vector<double> &objectives) override;
    void init_next_pop() override;
    bool update(const Environment::Ptr&);

    NEAT::Genome loadInd(short int genomeID);
    std::vector<int> listInds();

    void loadController(int ind, std::vector<double> &weights, std::vector<double> &biases);

private:
    std::unique_ptr<NEAT::Population> morph_population;

    NEAT::Parameters params;

    std::vector<Eigen::VectorXd> archive;

protected:
    NEAT::RNG rng;
    int currentIndIndex;

};

}

#endif //MorphoNeuro_H
