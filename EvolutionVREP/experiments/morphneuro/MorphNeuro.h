#ifndef MorphNeuro_H
#define MorphNeuro_H

#include "ARE/EA.h"
#include "ARE/CPPNGenome.h"
#include "CPPNIndividual.h"
#include "ARE/Morphology_CPPNMatrix.h"

#include "eigen3/Eigen/Core"

namespace are {

class MorphNeuro : public EA
{
public:
    MorphNeuro() : EA(){}
    MorphNeuro(const settings::ParametersMapPtr& param) : EA(param){}
    ~MorphNeuro() override {}

    void init() override;
    void initPopulation();
    void epoch() override;
    bool is_finish() override;
    void setObjectives(size_t indIdx, const std::vector<double> &objectives) override;
    void init_next_pop() override;
    NEAT::Genome loadInd(short int genomeID);
    std::vector<int> listInds();
    //bool finish_eval() override;

private:
    std::unique_ptr<NEAT::Population> morph_population;
    std::unique_ptr<NEAT::Population> contr_population;

    NEAT::Parameters params;
    NEAT::Parameters paramsCtrl;

    std::vector<Eigen::VectorXd> archive;

protected:
    NEAT::RNG rng;
    int currentIndIndex = 0;

};

}

#endif //MorphNeuro_H
