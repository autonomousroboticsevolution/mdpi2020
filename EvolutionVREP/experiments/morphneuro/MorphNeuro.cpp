#include "MorphNeuro.h"
#include <iostream>
#include <fstream>
#include <string>
#include "ManLog.h"
#include <map>

using namespace are;

void MorphNeuro::init()
{
    params = NEAT::Parameters();
    /// Set parameters for NEAT
    unsigned int pop_size = settings::getParameter<settings::Integer>(parameters,"#populationSize").value;
    params.EliteFraction = 0.05;
    params.PopulationSize = pop_size;
    params.DynamicCompatibility = true;
    params.CompatTreshold = 2.0;
    params.YoungAgeTreshold = 15;
    params.SpeciesMaxStagnation = 100;
    params.OldAgeTreshold = 50;
    params.MinSpecies = 5;
    params.MaxSpecies = 10;
    params.RouletteWheelSelection = false;

    params.MutateRemLinkProb = 0.02;
    params.RecurrentProb = 0.0;
    params.OverallMutationRate = 0.05;
    params.MutateAddLinkProb = 0.05;
    params.MutateAddNeuronProb = 0.01;
    params.MutateWeightsProb = 0.50;
    params.MaxWeight = 8.0;
    params.WeightMutationMaxPower = 0.2;
    params.WeightReplacementMaxPower = 1.0;

    params.MutateActivationAProb = 0.0;
    params.ActivationAMutationMaxPower = 0.5;
    params.MinActivationA = 0.05;
    params.MaxActivationA = 6.0;

    params.MutateNeuronActivationTypeProb = 0.03;

    // Crossover
    // params.SurvivalRate = 0.00;
    // params.CrossoverRate = 0.00;
    params.CrossoverRate = 0.70;
    // params.InterspeciesCrossoverRate = 0.00;

    params.ActivationFunction_SignedSigmoid_Prob = 0.0;
    params.ActivationFunction_UnsignedSigmoid_Prob = 0.0;
    params.ActivationFunction_Tanh_Prob = 1.0;
    params.ActivationFunction_TanhCubic_Prob = 0.0;
    params.ActivationFunction_SignedStep_Prob = 1.0;
    params.ActivationFunction_UnsignedStep_Prob = 0.0;
    params.ActivationFunction_SignedGauss_Prob = 1.0;
    params.ActivationFunction_UnsignedGauss_Prob = 0.0;
    params.ActivationFunction_Abs_Prob = 0.0;
    params.ActivationFunction_SignedSine_Prob = 1.0;
    params.ActivationFunction_UnsignedSine_Prob = 0.0;
    params.ActivationFunction_Linear_Prob = 1.0;

    /////************************

    paramsCtrl = NEAT::Parameters();
//    unsigned int pop_size = settings::getParameter<settings::Integer>(parameters,"#populationSize").value;
    paramsCtrl.PopulationSize = pop_size;
    paramsCtrl.DynamicCompatibility = true;
    paramsCtrl.CompatTreshold = 2.0;
    paramsCtrl.YoungAgeTreshold = 15;
    paramsCtrl.SpeciesMaxStagnation = 100;
    paramsCtrl.OldAgeTreshold = 50;
    paramsCtrl.MinSpecies = 5;
    paramsCtrl.MaxSpecies = 10;
    paramsCtrl.RouletteWheelSelection = false;

    paramsCtrl.MutateRemLinkProb = 0.02;
    paramsCtrl.RecurrentProb = 0.0;
    paramsCtrl.OverallMutationRate = 0.05;
    paramsCtrl.MutateAddLinkProb = 0.05;
    paramsCtrl.MutateAddNeuronProb = 0.01;
    paramsCtrl.MutateWeightsProb = 0.50;
    paramsCtrl.MaxWeight = 8.0;
    paramsCtrl.WeightMutationMaxPower = 0.2;
    paramsCtrl.WeightReplacementMaxPower = 1.0;

    paramsCtrl.MutateActivationAProb = 0.0;
    paramsCtrl.ActivationAMutationMaxPower = 0.5;
    paramsCtrl.MinActivationA = 0.05;
    paramsCtrl.MaxActivationA = 6.0;

    paramsCtrl.MutateNeuronActivationTypeProb = 0.03;

    // Crossover
    // paramsCtrl.SurvivalRate = 0.00;
    // paramsCtrl.CrossoverRate = 0.00;
    paramsCtrl.CrossoverRate = 0.70;
    // paramsCtrl.InterspeciesCrossoverRate = 0.00;

    paramsCtrl.ActivationFunction_SignedSigmoid_Prob = 0.0;
    paramsCtrl.ActivationFunction_UnsignedSigmoid_Prob = 0.0;
    paramsCtrl.ActivationFunction_Tanh_Prob = 1.0;
    paramsCtrl.ActivationFunction_TanhCubic_Prob = 0.0;
    paramsCtrl.ActivationFunction_SignedStep_Prob = 1.0;
    paramsCtrl.ActivationFunction_UnsignedStep_Prob = 0.0;
    paramsCtrl.ActivationFunction_SignedGauss_Prob = 1.0;
    paramsCtrl.ActivationFunction_UnsignedGauss_Prob = 0.0;
    paramsCtrl.ActivationFunction_Abs_Prob = 0.0;
    paramsCtrl.ActivationFunction_SignedSine_Prob = 1.0;
    paramsCtrl.ActivationFunction_UnsignedSine_Prob = 0.0;
    paramsCtrl.ActivationFunction_Linear_Prob = 1.0;

    initPopulation();

}
/*////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void MorphNeuro::initPopulation()
{
    rng.Seed(randomNum->getSeed());

    NEAT::Genome morph_genome(0, 5, 10, 6, false, NEAT::SIGNED_SIGMOID, NEAT::SIGNED_SIGMOID, 0, params, 10);
    NEAT::Genome contr_genome(0, 7, 10, 1, false, NEAT::SIGNED_SIGMOID, NEAT::SIGNED_SIGMOID, 0, paramsCtrl, 2);
    randomNum->setSeed(time(0));
    // Morphology and contrller use the same seed
    int population_rand_int = randomNum->randInt(0, 10000);
    morph_population = std::make_unique<NEAT::Population>(morph_genome, params, true, 1.0, population_rand_int);
    // TO CHANGE
    contr_population = std::make_unique<NEAT::Population>(contr_genome, paramsCtrl, true, 1.0, population_rand_int);

    int manufacturabilityMethod = settings::getParameter<settings::Integer>(parameters,"#manufacturabilityMethod").value;

    bool isBootstrapPopulation = settings::getParameter<settings::Boolean>(parameters,"#isBootstrapEvolution").value;
    std::vector<int> robotList;
    if(isBootstrapPopulation) {
        robotList = listInds();
    }

    for (size_t i = 0; i < params.PopulationSize ; i++)
    {
        if(isBootstrapPopulation) {
            NEAT::Genome indGenome;
            indGenome = loadInd(robotList[i]);
            morph_population->AccessGenomeByIndex(i) = indGenome;  //individual genome
        }
// Controller bootstrap
        std::vector<Individual::Ptr> Morph_Contrl_Idv;
        for (size_t j = 0; j < params.PopulationSize; j ++){
            CPPNGenome::Ptr contrlGenome(new CPPNGenome(contr_population->AccessGenomeByIndex(j)));
            CPPNGenome::Ptr morphgenome(new CPPNGenome(morph_population->AccessGenomeByIndex(i)));
            CPPNIndividual::Ptr ind(new CPPNIndividual(morphgenome,contrlGenome));
            ind->set_parameters(parameters);
            ind->set_randNum(randomNum);
            population.push_back(ind);
        }
    }
}

void MorphNeuro::epoch(){
    int idv_count = 0;
    std::vector<double> fitness_log_all;
    fitness_log_all.empty();
    for(const auto& ind : population){
        idv_count++;
        fitness_log_all.push_back(ind->getObjectives()[0]);
    }
    int pop_size = settings::getParameter<settings::Integer>(parameters,"#populationSize").value;
    double best_fitness = 0.0;
    int idx_counter = 0;
    int best_idx;
    int gen_counter;
    std::vector<double> idx_to_keep;
    idx_to_keep.empty();
    if (idv_count > pop_size){
        for(int i = 0; i < idv_count; i++){
            if(i != 0 && i % pop_size == 0){
                idx_to_keep.push_back(best_idx);
                best_fitness = 0;
            }else{
                double current_fitness = fitness_log_all[i];
                if(current_fitness > best_fitness){
                    best_fitness = current_fitness;
                    best_idx = i;
                }
            }
        }
        idx_to_keep.push_back(best_idx);
        std::cout<<"idx_to_keep " << idx_to_keep.size()<<std::endl;
        std::vector<Individual::Ptr> temp_population;
        for(int i = 0; i < idx_to_keep.size(); i++){
            temp_population.push_back(population[idx_to_keep[i]]);
        }
        population = temp_population;
        int indCounter = 0;
        for(const auto& ind : population){
            morph_population->AccessGenomeByIndex(indCounter).SetFitness(ind->getObjectives()[0]);
            contr_population->AccessGenomeByIndex(indCounter).SetFitness(ind->getObjectives()[0]);
            indCounter++;
        }
        morph_population->Epoch();
        contr_population->Epoch();
        std::cout<<"multi bootstrapping "<<std::endl;
        std::cout<<"population size " << population.size()<<std::endl;
    }
    else{
        int indCounter = 0;
        for(const auto& ind : population){
            morph_population->AccessGenomeByIndex(indCounter).SetFitness(ind->getObjectives()[0]);
            contr_population->AccessGenomeByIndex(indCounter).SetFitness(ind->getObjectives()[0]);
            indCounter++;
        }
        morph_population->Epoch();
        contr_population->Epoch();
    }
}
*//////////////////////////////////////////////////////////////////////////////////////////////

void MorphNeuro::initPopulation()
{
    rng.Seed(randomNum->getSeed());

    NEAT::Genome morph_genome(0, 5, 10, 6, false, NEAT::SIGNED_SIGMOID, NEAT::SIGNED_SIGMOID, 0, params, 10);
    NEAT::Genome contr_genome(0, 7, 10, 1, false, NEAT::SIGNED_SIGMOID, NEAT::SIGNED_SIGMOID, 0, paramsCtrl, 2);
    randomNum->setSeed(time(0));
    // Morphology and contrller use the same seed
    int population_rand_int = randomNum->randInt(0, 10000);
    morph_population = std::make_unique<NEAT::Population>(morph_genome, params, true, 1.0, population_rand_int);
    // TO CHANGE
    contr_population = std::make_unique<NEAT::Population>(contr_genome, paramsCtrl, true, 1.0, population_rand_int);
    int manufacturabilityMethod = settings::getParameter<settings::Integer>(parameters,"#manufacturabilityMethod").value;

    bool isBootstrapPopulation = settings::getParameter<settings::Boolean>(parameters,"#isBootstrapEvolution").value;
    std::vector<int> robotList;
    if(isBootstrapPopulation) {
        robotList = listInds();
    }

    for (size_t i = 0; i < params.PopulationSize ; i++)
    {
        if(isBootstrapPopulation) {
            NEAT::Genome indGenome;
            indGenome = loadInd(robotList[i]);
            morph_population->AccessGenomeByIndex(i) = indGenome;  //individual genome
        }

        CPPNGenome::Ptr contrlGenome(new CPPNGenome(contr_population->AccessGenomeByIndex(i)));
        CPPNGenome::Ptr morphgenome(new CPPNGenome(morph_population->AccessGenomeByIndex(i)));
//        EmptyGenome::Ptr no_gen(new EmptyGenome);
//        CPPNIndividual::Ptr ind(new CPPNIndividual(morphgenome,no_gen));
        CPPNIndividual::Ptr ind(new CPPNIndividual(morphgenome,contrlGenome));

        ind->set_parameters(parameters);
        ind->set_randNum(randomNum);
        population.push_back(ind);
    }
}

void MorphNeuro::epoch(){
    int indCounter = 0; /// \todo EB: There must be a better way to do this!
    for(const auto& ind : population){
        morph_population->AccessGenomeByIndex(indCounter).SetFitness(ind->getObjectives()[0]);
        contr_population->AccessGenomeByIndex(indCounter).SetFitness(ind->getObjectives()[0]);
        indCounter++;
    }
    morph_population->Epoch();
    contr_population->Epoch();

}




void MorphNeuro::setObjectives(size_t indIdx, const std::vector<double> &  objectives){
    currentIndIndex = indIdx;
    population[indIdx]->setObjectives(objectives);
}

void MorphNeuro::init_next_pop(){
    int manufacturabilityMethod = settings::getParameter<settings::Integer>(parameters,"#manufacturabilityMethod").value;
    population.clear();
    int pop_size = settings::getParameter<settings::Integer>(parameters,"#populationSize").value;
    for (int i = 0; i < pop_size ; i++)
    {
        CPPNGenome::Ptr morphgenome(new CPPNGenome(morph_population->AccessGenomeByIndex(i)));
        CPPNGenome::Ptr contrgenome(new CPPNGenome(contr_population->AccessGenomeByIndex(i)));
        CPPNIndividual::Ptr ind(new CPPNIndividual(morphgenome,contrgenome)); //individual combination, fitness related
        ind->set_parameters(parameters);
        population.push_back(ind);
    }
}

bool MorphNeuro::is_finish()
{
    unsigned int maxGenerations = settings::getParameter<settings::Integer>(parameters,"#numberOfGeneration").value;
    return get_generation() > maxGenerations;
}

NEAT::Genome MorphNeuro::loadInd(short genomeID)
{
    std::string loadExperiment = settings::getParameter<settings::String>(parameters,"#loadExperiment").value;
    std::cout << "Loading genome: " << genomeID << "!" << std::endl;
    std::stringstream filepath;
    filepath << loadExperiment << "/morphGenome" << genomeID;
    NEAT::Genome indGenome(filepath.str().c_str());
    return indGenome;
}

std::vector<int> MorphNeuro::listInds()
{
    std::vector<int> robotList;
    // This code snippet was taken from: https://www.gormanalysis.com/blog/reading-and-writing-csv-files-with-cpp/
    std::string loadExperiment = settings::getParameter<settings::String>(parameters,"#loadExperiment").value;
    std::string bootstrapFile = settings::getParameter<settings::String>(parameters,"#bootstrapFile").value;
    // Create an input filestream
    std::ifstream myFile(loadExperiment+bootstrapFile);
    if(!myFile.is_open()) throw std::runtime_error("Could not open file");
    std::string line;
    int val;

    while(std::getline(myFile, line)){
        // Create a stringstream of the current line
        std::stringstream ss(line);
        // Keep track of the current column index
        int colIdx = 0;
        // Extract each integer
        while(ss >> val) {
            // Add the current integer to the 'colIdx' column's values vector
            robotList.push_back(val);
            // If the next token is a comma, ignore it and move on
            if(ss.peek() == ',') ss.ignore();
            // Increment the column index
            colIdx++;
        }
    }
    // Close file
    myFile.close();
    return robotList;
}

/*
bool MorphNeuro::finish_eval()
{
    float tPos[3];
    tPos[0] = settings::getParameter<settings::Double>(parameters,"#target_x").value;
    tPos[1] = settings::getParameter<settings::Double>(parameters,"#target_y").value;
    tPos[2] = settings::getParameter<settings::Double>(parameters,"#target_z").value;
    double fTarget = settings::getParameter<settings::Double>(parameters,"#FTarget").value;
    double arenaSize = settings::getParameter<settings::Double>(parameters,"#arenaSize").value;

    auto distance = [](float* a,float* b) -> double
    {
        return std::sqrt((a[0] - b[0])*(a[0] - b[0]) +
                         (a[1] - b[1])*(a[1] - b[1]) +
                         (a[2] - b[2])*(a[2] - b[2]));
    };

//    std::cout << "currentIndIndex: " << currentIndIndex << std::endl;

    int handle = (morph_population->AccessGenomeByIndex(currentIndIndex).get_morphology())->getMainHandle();
    float pos[3];
    simGetObjectPosition(handle,-1,pos);
    double dist = distance(pos,tPos)/sqrt(2*arenaSize*arenaSize);

    if(dist < fTarget){
        std::cout << "STOP !" << std::endl;
    }

    return  dist < fTarget;
}*/

