# Experimental code

## Bootstrapping artificial evolution to design robots for autonomous fabrication

In this directory you will find the code to replicate the experiments shown in the paper *Bootstrapping artificial evolution to design robots for autonomous fabrication*

## Experiments

Experiments for each section of the paper are described below:

* Section 3.1.3 - *lc_nsms* and *lc_gsms*
* Section 3.3.1 - *morphoneuro* and *pop_morph_nipes*
* Section 3.4.1

## Launching experiments

You can refer to this [page](https://bitbucket.org/autonomousroboticsevolution/evolutionary_robotics_framework/wiki/Defining%20an%20experiment%20within%20the%20ARE%20Framework) for more information to launch the experiments.
