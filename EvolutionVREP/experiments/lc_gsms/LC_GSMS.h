#ifndef LC_GSMS_H
#define LC_GSMS_H

#include "ARE/EA.h"
#include "ARE/CPPNGenome.h"
#include "ARE/Morphology_CPPNMatrix.h"
#include "CPPNIndividual.h"

#include "eigen3/Eigen/Core"

#include "ARE/learning/Novelty.hpp"

namespace are {

class LC_GSMS : public EA
{
public:
    LC_GSMS() : EA(){}
    LC_GSMS(const settings::ParametersMapPtr& param) : EA(param){}
    ~LC_GSMS() override {}

    void init() override;
    void initPopulation();
    void epoch() override;
    bool is_finish() override;
    void setObjectives(size_t indIdx, const std::vector<double> &objectives) override;
    void init_next_pop() override;
    std::vector<double> graphDistances(const std::vector<std::vector<std::vector<int>>> &graph_desc,
                                       const std::vector<std::vector<std::vector<std::vector<int>>>> &graphArchive,
                                       const std::vector<std::vector<std::vector<std::vector<int>>>> &pop,
                                       std::vector<size_t> & sorted_pop_indexes);

    double estimateGraphDistance(const std::vector<std::vector<std::vector<int>>> &ind_graph, const std::vector<std::vector<std::vector<int>>> &neighbour_graph);
    void updateArchive(const std::vector<std::vector<std::vector<int>>> &ind_graph,
                       double ind_nov,
                       std::vector<std::vector<std::vector<std::vector<int>>>> &graphArchive,
                       const misc::RandNum::Ptr &rn);

    NEAT::Genome loadInd(short int genomeID);
    std::vector<int> listInds();

private:
    std::unique_ptr<NEAT::Population> morph_population;

    NEAT::Parameters params;

    std::vector<Eigen::VectorXd> archive;

protected:
    NEAT::RNG rng;
    int currentIndIndex;

    std::vector<std::vector<std::vector<std::vector<int>>>> graphArchive;

};

}

#endif //LC_GSMS_H
