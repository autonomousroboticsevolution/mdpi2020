#ifndef Morphology_CPPNMatrix_H
#define Morphology_CPPNMatrix_H

#include "ARE/Morphology.h"

#include "PolyVox/RawVolume.h"
#include "PolyVox/MarchingCubesSurfaceExtractor.h"
#include "eigen3/Eigen/Core"

#include "math.h"

/// \todo EB: Should this be here?
enum VoxelType
{
    BONE,
    WHEEL,
    SENSOR,
    JOINT,
    CASTER
};

namespace are {

class Morphology_CPPNMatrix : public Morphology
{
public:
    Morphology_CPPNMatrix(const settings::ParametersMapPtr &param) : Morphology(param){}

    struct AREVoxel{
        uint8_t bone;
        uint8_t wheel;
        uint8_t sensor;
        uint8_t joint;
        uint8_t caster;
    };

    struct OrganSpec
    {
        int handle;
        int organType;
        int connectorHandle;
        int gripperHandle;
        std::vector<float> organPos;
        std::vector<float> connectorPos;
        std::vector<float> organOri;
        std::vector<float> connectorOri;
        bool organInsideSkeleton;
        bool organColliding;
        bool organGoodOrientation;
        bool organGripperAccess;
        std::vector<int> objectHandles; // For collision detection purpose
    };

    class RobotManRes{
    public:
        bool noCollisions;
        bool noBadOrientations;
        bool isGripperAccess;
        short int wheelsRepressed;
        short int sensorsRepressed;
        short int jointsRepressed;
        short int casterRepressed;
        // Constructor
        RobotManRes(){
            noCollisions = true;
            noBadOrientations = true;
            isGripperAccess = true;
            wheelsRepressed = 0;
            sensorsRepressed = 0;
            jointsRepressed = 0;
            casterRepressed = 0;
        }
        std::vector<bool> getResVector(){
            std::vector<bool> resVector;
            resVector.push_back(noCollisions);
            resVector.push_back(noBadOrientations);
            resVector.push_back(isGripperAccess);
            resVector.push_back(wheelsRepressed);
            resVector.push_back(sensorsRepressed);
            resVector.push_back(jointsRepressed);
            resVector.push_back(casterRepressed);
            return resVector;
        }
    };
    RobotManRes robotManRes;

    Morphology::Ptr clone() const override
        {return std::make_shared<Morphology_CPPNMatrix>(*this);}

    void create() override;
    void createAtPosition(float,float,float) override;
    void setPosition(float,float,float);

    /////////////////////////////
    ///// Create morphology /////
    /////////////////////////////
    /**
     * @brief Decodes the genome (CPPN --> Matrix)
     */
    void genomeDecoder(PolyVox::RawVolume<AREVoxel>& areMatrix, NEAT::NeuralNetwork &cppn);
    /**
     * @brief This method gets all the verices and indices of the mesh generated by PolyVox.
     * This list is imported to V-REP.
     */
    bool getIndicesVertices(PolyVox::Mesh<PolyVox::Vertex<uint8_t>>& decodedMesh, std::vector<float>& vertices, std::vector<int>& indices);
    /**
     * @brief Create the head organ. This should be the first organ created.
     */
    void createHead();
    /**
     * @brief This method generates the complete skeleton without alterations of the robot from the matrix of voxels.
     * \todo This method might not be necessary!
     */
    void generateSkeleton(PolyVox::RawVolume<AREVoxel>& areMatrix, PolyVox::RawVolume<uint8_t>& skeletonMatrix, VoxelType _voxelType);
    /**
     * @brief This method loads model, creates force sensor sets position and orientation and assigns parent.
     */
    void createOrgan(OrganSpec& organ);
    /**
     * @brief Creates a temporal gripper. The isOrganColliding method checks this gripper is not colliding.
     */
    void createTemporalGripper(OrganSpec& organ);
    /**
     * @brief Takes the last three outputs of the network and converts them into rotations (radians) in x, y and z.
     * \todo EB: This method needs updating. Only one output required.
     */
    void setOrganOrientation(NEAT::NeuralNetwork &cppn, OrganSpec& organ);
    /**
     * @brief Creates a model to only visualize the vale connector
     */
    void createMaleConnector(OrganSpec& organ);

    /**
     * @brief Removes the gripper created with createTemporalGripper
     */
    static void removeGripper(int gripperHandle);

    ////////////////////////////////////////////
    /////  Manufacturability testing stuff /////
    ////////////////////////////////////////////
    /// \todo EB: Might be good idea to move this testing to a class
    /**
     * @brief This method tests that robot as a whole.
     */
    void testRobot(PolyVox::RawVolume<uint8_t>& skeletonMatrix);
    /**
     * @brief Tests each component (organ) in the robot.
     */
    void testComponents(PolyVox::RawVolume<uint8_t>& skeletonMatrix);
    /**
     * @brief If a component fails any manufacturability test that component is removed from the final ns.
     */
    void geneRepression();

    /// Not used...
    void removeRobot();
    /// Measure manufacturability score
    void manufacturabilityScore();

    /**
     * @brief This method check if a specific organ (organHandle) is colliding with other components.ding
     */
    bool IsOrganColliding(OrganSpec& organ);
    /**
     * @brief This method checks if the organ is within the skeleton.
     * The main difference between IsOrganColloding and IsOrganConnected is that IsOrganColliding does not check
     * if one component is inside of a second component.
     */
    bool IsOrganInsideSkeleton(PolyVox::RawVolume<uint8_t>& skeletonMatrix, int organHandle);
    /**
     * @brief Checks if an organ has the correct orientation.
     * \todo EB: This method this updating. New method only requires one rotation.
     */
    bool isOrganGoodOrientation(OrganSpec& organ);
    /**
     * Checks if the gripper for the specified organ is colliding.
     */
    bool isGripperColliding(int gripperHandle);

    ////////////////////////////////////////
    ///// Fixing phenotype techniques //////
    ////////////////////////////////////////
    /**
     * @brief This method makes space for the Head Organ by removing voxels.
     */
    void emptySpaceForHead(PolyVox::RawVolume<uint8_t>& skeletonMatrix);
    /**
     * @brief This method creates a predifines skeleton.
     */
    void createSkeletonBase(PolyVox::RawVolume<uint8_t>& skeletonMatrix);

    ///////////////////////////
    ///// Surface methods /////
    ///////////////////////////
    /**
     * @brief This method is used to explore the entire skeleton regions in order to find the voxels adjecent to the
     * surface.
     */
    void exploreSkeleton(PolyVox::RawVolume<uint8_t>& skeletonMatrix, PolyVox::RawVolume<bool>& visitedVoxels, int32_t posX, int32_t posY, int32_t posZ, int surfaceCounter);
    /**
     * @brief This recursive method records all the voxels adjecent to the surface of the skeleton.
     * @param skeletonMatrix
     */
    void findSkeletonSurface(PolyVox::RawVolume<uint8_t>& skeletonMatrix);
    /**
     * @brief This method generates organs only in specific regions of the surface of the skeleton according to the cppn.
     */
    void generateOrgans(NEAT::NeuralNetwork &cppn);
    /**
     * @brief This method generates the orientation of the organ accoriding to the "normal" of the surface
     * In reality, it takes the position of the last outer voxel. In other words, it generates orientations in
     * intervals of 45o.
     */
    void generateOrientations(int x, int y, int z, OrganSpec& organ);

    /////////////////////////////////////////////
    ///// Methods handling skeleton regions /////
    /////////////////////////////////////////////
    /**
     * @brief This method counts the number of regions in the skeletonMatrix
     * @param skeletonMatrix
     */
    void skeletonRegionCounter(PolyVox::RawVolume<uint8_t>& skeletonMatrix);
    /**
     * @brief This method explore the skeleton matrix
     */
    void exploreSkeletonRegion(PolyVox::RawVolume<uint8_t>& skeletonMatrix, PolyVox::RawVolume<bool>& visitedVoxels, int32_t posX, int32_t posY, int32_t posZ, int regionCounter);
    /**
     * @brief If there is more than one region in the skeleton delete the smaller regions.
     */
    void removeSkeletonRegions(PolyVox::RawVolume<uint8_t>& skeletonMatrix);
    /**
     * @brief This method explore the organs matrix
     */
    void organRegionCounter(PolyVox::RawVolume<AREVoxel>& areMatrix, VoxelType voxelType);
    /**
     * @brief This method explore the skeleton matrix
     */
    void exploreOrganRegion(PolyVox::RawVolume<AREVoxel>& areMatrix, PolyVox::RawVolume<bool>& visitedVoxels, int32_t posX, int32_t posY, int32_t posZ, int regionCounter, VoxelType voxelType);

    void getFinalSkeletonVoxels(PolyVox::RawVolume<uint8_t>& skeletonMatrix);

    //////////////////////////////////
    ///// Miscellanous functions /////
    //////////////////////////////////

    /**
     * @brief Export mesh file (stl) from a list of vertices and indices.
     * \todo EB: We might not want this method here and this should be in logging instead.
     */
    void exportMesh(int loadInd, std::vector<float> vertices, std::vector<int> indices);
    /**
     * @brief Export the robot as ttm model
     * \todo EB: We might not want this method here and this should be in logging instead.
     */
    void exportRobotModel(int indNum);
    /**
     * @brief This method renders the matrix of a specific organ. Useful for debugging.
     */
    void tempVisualizeMatrix(NEAT::NeuralNetwork &neuralNetwork, VoxelType _voxelType, float posX, float posY, float posZ);
    /**
     * @brief This method create the AREPuck robot with the matrix body plan.
     */
    void createAREPuck(PolyVox::RawVolume<uint8_t>& skeletonMatrix);
    /**
     * @brief This method create the AREPotato robot with the matrix body plan.
     */
    void createAREPotato(PolyVox::RawVolume<uint8_t>& skeletonMatrix);
    /**
     * @brief This method create the ARETricycle robot with the matrix body plan.
     */
    void createARETricyle(PolyVox::RawVolume<uint8_t>& skeletonMatrix);

    ///////////////////////////////
    ///// Setters and getters /////
    ///////////////////////////////
    std::vector<bool> getRobotManRes(){return robotManRes.getResVector();};
    NEAT::NeuralNetwork getGenome(){return nn;};
    void setGenome(NEAT::NeuralNetwork genome){nn = genome;};
    double getManScore(){return manScore;};
    void setManScore(double ms){ manScore = ms;};

    /// Getters for descriptors.
    /// \todo EB: There must be a better way to retrieve descriptor. Perhaps as the descriptor as a whole?
    Eigen::VectorXd getMorphDesc(){return indDesc.cartDesc.cartDesc;};
    std::vector<std::vector<std::vector<int>>> getGraphMatrix(){return indDesc.matDesc.graphMatrix;};
    Eigen::VectorXd getSymDesc(){return indDesc.symDesc.symDesc;};


private:
    ///////////////////////
    ///// Descriptors /////
    ///////////////////////

    class CartDesc
    {
    public:

        const int ORGANTRAITLIMIT = 5;
        float robotWidth; // X
        float robotDepth; // Y
        float robotHeight; // Z
        int voxelNumber;
        int wheelNumber;
        int sensorNumber;
        int casterNumber;
        int jointNumber;
        Eigen::VectorXd cartDesc;
        // Constructor
        CartDesc(){
            cartDesc.resize(8);
            robotWidth = 0;
            robotDepth = 0;
            robotHeight = 0;
            voxelNumber = 0;
            wheelNumber = 0;
            sensorNumber = 0;
            casterNumber = 0;
            jointNumber = 0;
            setCartDesc();
        }
        void setCartDesc(){
            cartDesc(0) = robotWidth / MATRIX_SIZE_M;
            cartDesc(1) = robotDepth / MATRIX_SIZE_M;
            cartDesc(2) = robotHeight / MATRIX_SIZE_M;
            cartDesc(3) = (double) voxelNumber / VOXELS_NUMBER;
            cartDesc(4) = (double) wheelNumber / ORGANTRAITLIMIT;
            cartDesc(5) = (double) sensorNumber / ORGANTRAITLIMIT;
            cartDesc(6) = (double) jointNumber / ORGANTRAITLIMIT;
            cartDesc(7) = (double) casterNumber / ORGANTRAITLIMIT;
        }
        void countOrgans( std::vector<OrganSpec> _organSpec){
            for(std::vector<OrganSpec>::iterator it = _organSpec.begin(); it != _organSpec.end(); it++){
                if(it->organType == 1)
                    wheelNumber++;
                if(it->organType == 2)
                    sensorNumber++;
                if(it->organType == 3)
                    jointNumber++;
                if(it->organType == 4)
                    casterNumber++;
            }
        }
        void getSkeletonDimmensions(PolyVox::RawVolume<uint8_t>& skeletonMatrix){
            uint8_t uVoxelValue = 0;
            std::vector<float> voxCoordX;
            std::vector<float> voxCoordY;
            std::vector<float> voxCoordZ;
            auto region = skeletonMatrix.getEnclosingRegion();
            // Create list of coordinates
            for(int32_t z = region.getLowerZ()+1; z < region.getUpperZ(); z += 1) {
                for (int32_t y = region.getLowerY() + 1; y < region.getUpperY(); y += 1) {
                    for (int32_t x = region.getLowerX() + 1; x < region.getUpperX(); x += 1) {
                        uVoxelValue = skeletonMatrix.getVoxel(x,y,z);
                        if(uVoxelValue > 0){
                            voxCoordX.push_back(x);
                            voxCoordY.push_back(y);
                            voxCoordZ.push_back(z);
                        }
                    }
                }
            }
            // Get max and min elements
            auto maxX = *max_element(std::begin(voxCoordX),std::end(voxCoordX));
            auto maxY = *max_element(std::begin(voxCoordY),std::end(voxCoordY));
            auto maxZ = *max_element(std::begin(voxCoordZ),std::end(voxCoordZ));
            auto minX = *min_element(std::begin(voxCoordX),std::end(voxCoordX));
            auto minY = *min_element(std::begin(voxCoordY),std::end(voxCoordY));
            auto minZ = *min_element(std::begin(voxCoordZ),std::end(voxCoordZ));

            // Get dimmensions
            float xDiff = abs(maxX - minX) + 1;
            float yDiff = abs(maxY - minY) + 1;
            float zDiff = abs(maxZ - minZ) + 1;
            robotWidth = xDiff * VOXEL_REAL_SIZE;
            robotDepth = yDiff * VOXEL_REAL_SIZE;
            robotHeight = zDiff * VOXEL_REAL_SIZE;
        }
    };

    class MatDesc{
    public:
        std::vector<std::vector<std::vector<int>>> graphMatrix;
        // Constructor
        MatDesc(){
            graphMatrix.resize(MATRIX_SIZE + 1);
            for(int i = 0; i < MATRIX_SIZE + 1; i++){
                graphMatrix[i].resize(MATRIX_SIZE + 1);
                for(int j = 0; j < MATRIX_SIZE + 1; j++){
                    graphMatrix[i][j].resize(MATRIX_SIZE + 1);
                    for(int k = 0; k < MATRIX_SIZE + 1; k++){
                        graphMatrix[i][j][k] = 0;
                    }
                }
            }
        }
    };

    class SymDesc{
    public:
        int voxel[8], wheel[8], sensor[8], joint[8], caster[8];
        Eigen::VectorXd symDesc;
        SymDesc(){
            for(int i = 0; i < 8; i++){
                voxel[i] = 0; wheel[i] = 0; sensor[i] = 0; joint[i] = 0; caster[i] = 0;
            }
            symDesc.resize(40);
            setSymDesc();
        }
        void setSymDesc(){
            short counter = 0;
            for(int i = 0; i < 8; i++){
                symDesc(counter) = (double) voxel[i] / 274; counter++; /// \todo EB: We need to define this constants elsewhere.
                symDesc(counter) = (double) wheel[i] / 3.2; counter++;
                symDesc(counter) = (double) sensor[i] / 3.2; counter++;
                symDesc(counter) = (double) joint[i] / 3.2; counter++;
                symDesc(counter) = (double) caster[i] / 3.2; counter++;
            }
        }
        void setVoxelQuadrant(short signed int x, short signed int y, short signed int z, short unsigned int componentType){
            if(x >= 0 && y >= 0 && z >= 0){
                if(componentType == 0)
                    voxel[0]++;
                else if(componentType == 1)
                    wheel[0]++;
                else if(componentType == 2)
                    sensor[0]++;
                else if(componentType == 3)
                    joint[0]++;
                else if(componentType == 4)
                    caster[0]++;

            } else if(x < 0 && y >= 0 && z >= 0){
                if(componentType == 0)
                    voxel[1]++;
                else if(componentType == 1)
                    wheel[1]++;
                else if(componentType == 2)
                    sensor[1]++;
                else if(componentType == 3)
                    joint[1]++;
                else if(componentType == 4)
                    caster[1]++;

            } else if(x < 0 && y < 0 && z >= 0){
                if(componentType == 0)
                    voxel[2]++;
                else if(componentType == 1)
                    wheel[2]++;
                else if(componentType == 2)
                    sensor[2]++;
                else if(componentType == 3)
                    joint[2]++;
                else if(componentType == 4)
                    caster[2]++;

            } else if(x >= 0 && y < 0 && z >= 0){
                if(componentType == 0)
                    voxel[3]++;
                else if(componentType == 1)
                    wheel[3]++;
                else if(componentType == 2)
                    sensor[3]++;
                else if(componentType == 3)
                    joint[3]++;
                else if(componentType == 4)
                    caster[3]++;

            } else if(x >= 0 && y >= 0 && z < 0){
                if(componentType == 0)
                    voxel[4]++;
                else if(componentType == 1)
                    wheel[4]++;
                else if(componentType == 2)
                    sensor[4]++;
                else if(componentType == 3)
                    joint[4]++;
                else if(componentType == 4)
                    caster[4]++;

            } else if(x < 0 && y >= 0 && z < 0){
                if(componentType == 0)
                    voxel[5]++;
                else if(componentType == 1)
                    wheel[5]++;
                else if(componentType == 2)
                    sensor[5]++;
                else if(componentType == 3)
                    joint[5]++;
                else if(componentType == 4)
                    caster[5]++;

            } else if(x < 0 && y < 0 && z < 0){
                if(componentType == 0)
                    voxel[6]++;
                else if(componentType == 1)
                    wheel[6]++;
                else if(componentType == 2)
                    sensor[6]++;
                else if(componentType == 3)
                    joint[6]++;
                else if(componentType == 4)
                    caster[6]++;

            } else if(x >= 0 && y < 0 && z < 0){
                if(componentType == 0)
                    voxel[7]++;
                else if(componentType == 1)
                    wheel[7]++;
                else if(componentType == 2)
                    sensor[7]++;
                else if(componentType == 3)
                    joint[7]++;
                else if(componentType == 4)
                    caster[7]++;

            }
        }
    };

    class Descriptors{
    public:
        CartDesc cartDesc;
        MatDesc matDesc;
        SymDesc symDesc;
    };

private:
    NEAT::NeuralNetwork nn;

    /////////////////////
    ///// Constants /////
    /////////////////////
    constexpr static const float VOXEL_SIZE = 0.0009; //m³ - 0.9mm³
    // WAS 4 -> 3.6mm
    // 6 -> 5.4mm
    // 11 -> 9.9mm (EB: with this value there is no stack overflow!)
    static const int VOXEL_MULTIPLIER = 22;
    constexpr static const float VOXEL_REAL_SIZE = VOXEL_SIZE * static_cast<float>(VOXEL_MULTIPLIER);
    static const int MATRIX_SIZE = (264 / VOXEL_MULTIPLIER);
    const int MATRIX_HALF_SIZE = MATRIX_SIZE / 2;
    const float SHAPE_SCALE_VALUE = VOXEL_REAL_SIZE; // results into 23.76x23.76x23.76 cm³ - in reality is 28x28x25 cm³
    static const int VOXELS_NUMBER = MATRIX_SIZE * MATRIX_SIZE *MATRIX_SIZE;
    static const int MAX_NUM_ORGANS = 10;
    constexpr static const float MATRIX_SIZE_M = MATRIX_SIZE * VOXEL_REAL_SIZE;

    const int EMPTYVOXEL = 0;
    const int FILLEDVOXEL = 255;

    // Skeleton dimmesions in voxels
    const int xHeadUpperLimit = 2;
    const int xHeadLowerLimit = -2;
    const int yHeadUpperLimit = 2;
    const int yHeadLowerLimit = -2;
    // Parameters for skeleton base
    const int skeletonBaseThickness = 1;
    const int skeletonBaseHeight = 2;

    unsigned int id;
    // Variables used to contain handles.
    std::vector<OrganSpec> _organSpec;
    int numSkeletonVoxels;

    std::vector<std::vector<std::vector<int>>> skeletonSurfaceCoord;
    std::vector<std::vector<std::vector<int>>> skeletonRegionCoord;

    std::vector<std::vector<std::vector<int>>> wheelRegionCoord;
    std::vector<std::vector<std::vector<int>>> sensorRegionCoord;
    std::vector<std::vector<std::vector<int>>> jointRegionCoord;
    std::vector<std::vector<std::vector<int>>> casterRegionCoord;

    double manScore;


    Descriptors indDesc;
};

}

#endif //Morphology_CPPNMatrix_H
